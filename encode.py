import sys, getopt
import cv2
from steganography import LSBSteganography, SpiralSteganography

from util import send_steganographic_image
from util import encrypt, decrypt

def getArgs(argv):
	try:
		args,_ = getopt.getopt(argv,"i:o:f:a:")
		if len(args) < 3:
			raise getopt.GetoptError("Usage:\n encode.py -i <inputfile> -f <file_to_hide> -o <outputfile> -a (optional) <algorithm to use - 'rowmajor'(default) or 'spiral'")

	except getopt.GetoptError:
		print("Usage:\nencode.py -i <inputfile> -f <file_to_hide> -o <outputfile> -a (optional) <algorithm to use - 'rowmajor'(default) or 'spiral'")
		sys.exit(2)

	algo = "rowmajor"
	for opt, arg in args:
		if opt == "-i":
			inputFile = arg
		elif opt == "-o":
			outputFile = arg
		elif opt == "-f":
			message = arg
		elif opt == "-a":
			algo = arg.lower()

	return inputFile, outputFile, message, algo

def main(argv):
	in_f, out_f, message, algo = getArgs(argv)
	in_img = cv2.imread(in_f)

	if algo == "spiral":
		steg = SpiralSteganography(in_img)
	else:
		steg = LSBSteganography(in_img)

	lossy_formats = ["jpeg", "jpg"]

	out_f, out_ext = out_f.split(".")
	if out_ext in lossy_formats:
		out_f = out_f + ".png"
		print("Output file changed to ", out_f)

	data = open(message, "rb").read()
	key, cipher = encrypt(data)

	text = key + cipher
	res = steg.encode_binary(text)
	cv2.imwrite(out_f, res)

	return out_f

if __name__ == "__main__":
	out_f = main(sys.argv[1:])
	print("Do you want to email the image? (y / n)")
	choice = input()

	if choice == "y":
		send_steganographic_image(out_f)
