from email.mime.multipart import MIMEMultipart
from email.mime.image import MIMEImage
from email.mime.text import MIMEText
import smtplib

from cryptography.fernet import Fernet

 
class Email:
	def __init__(self, smtp_server = 'smtp.gmail.com'):
		# setup the server
		self.server = smtplib.SMTP(smtp_server, 587)
		self.server.starttls()
 
		self.email = ""

	def login(self, email, passwd):
		# login using email and passwd
		self.server.login(email, passwd)
		self.email = email

	def send_image(self, to, subject, image_uri):
		# create message object to send
		msg = MIMEMultipart()
		msg['From'] = self.email
		msg['To'] = to
		msg['Subject'] = subject

		# Attach image
		msg.attach(MIMEImage(open(image_uri, 'rb').read()))
		
		# Finally send images
		print("Sending image...")
		self.server.sendmail(self.email, to, msg.as_string())
 
	def __del__(self):
		# clean the resources
		self.server.quit()


def encrypt(data):
	key = Fernet.generate_key()
	f = Fernet(key)
	encrypted_text = f.encrypt(data)

	print("key ", key)
	print("data ", encrypted_text)
	return key, encrypted_text

def decrypt(key, cipher_text):
	f = Fernet(key)
	data = f.decrypt(cipher_text).decode()	

	return data

def send_steganographic_image(image_uri):
	passwd = "***********"
	From = "termproject547@gmail.com"
	To = "sheetalgupta00001@gmail.com"
	Subject = "Sent from python"

	print("Enter the recipient: ")
	recipient = input()
	if recipient != "":
		To = recipient

	email = Email()
	email.login(From, passwd)
	email.send_image(to = To, image_uri = image_uri, subject = Subject)

	print("Successfully sent image to {}".format(To))

# text = "This is the original text"
# key, cipher = encrypt(text)
# print("key:", key, len(key), str(key.decode()))
# print("cipher:", cipher)

# original_text = decrypt(key, cipher)
# print("Original Text:", original_text) 