import cv2
import sys, getopt
from steganography import LSBSteganography, SpiralSteganography	

from util import encrypt, decrypt

def getArgs(argv):
	try:
		args,_ = getopt.getopt(argv,"i:o:a:")
		if len(args) < 2:
			raise getopt.GetoptError("")

	except getopt.GetoptError:
		print("Usage:\nencode.py -i <inputfile> -o <outputfile> -a (optional) <algorithm to use - 'rowmajor'(default) or 'spiral'")
		sys.exit(2)

	algo = "rowmajor"
	for opt, arg in args:
		if opt == "-i":
			inputFile = arg
		elif opt == "-o":
			outputFile = arg
		elif opt == "-a":
			algo = arg.lower()

	return inputFile, outputFile, algo

def splitkeydata(raw):
	key = raw[:44]
	data = raw[44:]

	return key, data

def main(argv):
	in_f, out_f, algo = getArgs(argv)
	in_img = cv2.imread(in_f)
	if algo == "spiral":
		steg = SpiralSteganography(in_img)
	else:
		steg = LSBSteganography(in_img)

	lossy_formats = ["jpeg", "jpg"]

	raw = steg.decode_binary()

	key, data = splitkeydata(raw)
	data = decrypt(key, data)

	with open(out_f, "w") as f:
	    f.write(data)

if __name__ == "__main__":
	main(sys.argv[1:])